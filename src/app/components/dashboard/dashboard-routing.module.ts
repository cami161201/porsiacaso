import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForoGuardGuard } from 'src/app/foro-guard.guard';
import { DashboardComponent } from './dashboard.component';
import { CrearForosComponent } from './foros/crear-foros/crear-foros.component';
import { ForosComponent } from './foros/foros.component';
import { VerForosComponent } from './foros/ver-foros/ver-foros.component';
import { InicioComponent } from './inicio/inicio.component';
import { UsuarioComponent } from './usuario/usuario.component';

const routes: Routes = [
  { path: '', component: DashboardComponent,
    children:
  [
    { path: '', component: InicioComponent },
    { path: 'foros', component: ForosComponent},
    { path: 'usuario', component: UsuarioComponent},
    { path: 'crear-foro/:id', component: CrearForosComponent, canActivate: [ForoGuardGuard]  },
    { path: 'ver-foro/:id', component: VerForosComponent, canActivate: [ForoGuardGuard]  },
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
