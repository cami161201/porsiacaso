import { Injectable } from '@angular/core';
import { foroI } from '../interfaces/all-interface.interface';

@Injectable({
  providedIn: 'root'
})
export class ForosService {

  listaForos!: foroI[];
  listaTemas : foroI[] = [];
  hayborrador : boolean = false;

  constructor() { }

  agregarNew(foro: foroI){
    console.log(foro);

    if(localStorage.getItem('Foros') === null){
      this.listaForos = [];
      this.listaForos.unshift(foro);
      localStorage.setItem('Foros', JSON.stringify(this.listaForos))
    } else {
      this.listaForos = JSON.parse(localStorage.getItem('Foros') || '');
      this.listaForos.unshift(foro);
      localStorage.setItem('Foros', JSON.stringify(this.listaForos))
    }

    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }

  agregarBorrador(foro: foroI){
    console.log(foro);

    if(sessionStorage.getItem('Foros') === null){
      this.listaForos = [];
      this.listaForos.unshift(foro);
      sessionStorage.setItem('Foros', JSON.stringify(this.listaForos))
    } else {
      this.listaForos = JSON.parse(sessionStorage.getItem('Foros') || '');
      this.listaForos.unshift(foro);
      sessionStorage.setItem('Foros', JSON.stringify(this.listaForos))
    }
  }

  //ERROR
  obtenerLocalStorage(){

    if (localStorage.getItem("Foros")?.length === 0){
      this.listaForos = []
    } else if (localStorage.getItem("Foros")?.length !== 0){
      let listaForos = JSON.parse(localStorage.getItem("Foros") || '');
    console.log(listaForos);

    this.listaForos = listaForos
    // console.log(this.listaForos, 'obtener LocalStorage');
    return listaForos
    }
    
  }


  obtenerUnForo(id: number){
    console.log('entra al servicio', id);

    this.listaTemas = this.obtenerLocalStorage();
    console.log(this.listaTemas);

    const encontrado = this.listaTemas.find(e => e.foroid === id)
    console.log(encontrado);

    return encontrado
  }

  obtenerUnForoBorrador(id: number){
    console.log('entra al servicio', id);

    this.listaTemas = JSON.parse(sessionStorage.getItem("Foros") || '');
    console.log(this.listaTemas);
    console.log(this.listaTemas);

    const encontrado = this.listaTemas.find(e => e.foroid === id)
    console.log(encontrado);

    return encontrado
  }

  //MOSTRAR BORRADOR
  recibirBorrador(foro: foroI){
    let foroB = foro
    return foroB
  }

}
