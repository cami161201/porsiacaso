export interface UsuarioI {
  usuario: string;
  pass1: string;
}

export interface UsuarioRegistradoI {
  nombre: string,
  correo: string,
  usuario: string,
  genero: string,
  pass1: string,
  pass2: string
}

export interface foroI {
  foroid: number,
  usuario: String,
  seccion: number,
  titulo: string,
  fecha: string,
  foro: string,
}

export interface comentariosI {
  comentid : number,
  foroid: number,
  usuario: string,
  fecha: fechaI,
  comentario: string,
}

interface fechaI {
  dia: string, 
  hora: string
}